class Song
	attr_accessor :name, :artist, :genre, :subgenre, :tags

	def initialize songString
		songString = songString.split(".")
		@name = songString[0].strip
		@artist = songString[1].strip
		genre_arr = songString[2].split(",")
		@genre = genre_arr[0].strip
		@subgenre = genre_arr[1].strip if !genre_arr[1].nil?
		if !songString[3].nil?
			@tags = songString[3].split(",") 
			@tags.each {|t| t.strip!}
		end
	end

	def printSong
		puts ""
		puts "Name: " + @name
		puts "Artist: " + @artist
		puts "Genre: " +@genre
		puts "Subgenre: " + @subgenre if !@subgenre.nil?
		print "Tags: ",@tags,"\n\n"
	end

	def dictionarize dictionary
		dictionary.each do |key, value|
			@tags += value if @artist == key
		end
	end
end

class MusicCollection
	attr_accessor :songs, :dictionary

	def initialize songString, dictionary = nil
		@songs = []
		songString.each_line {|str| @songs.push Song.new(str)}
		dictionarize(dictionary) if !dictionary.nil?
	end

	def printCollection
		@songs.each {|s| s.printSong}
	end

	def find *criteria
		song_arr = []
		return @songs if criteria.nil?
		if criteria[0].key? :filter
			temp = []
			@songs.each{|song| song_arr.push(temp.push(song)) if yield song}
		end
		criteria[0].each_pair do |key, value|
			if key != :filter
				song_arr.push(privateFind(key, value))
			end
		end
		return form_arr(song_arr)
	end

private

	def privateFind key, value
		song_arry = []
			@songs.each do |song|
				if key == :tags
					song_arry.push(song) if song.tags.include?(value)	
				else
					song_arry.push(song) if song.send(key) == value
				end
			end
		song_arry
	end

	def form_arr song_arr
		temp_arr = []
		song_arr[0].each do |i|
			bool = true
			song_arr.each do |j|
				bool = false if !j.include?(i)

			end
			temp_arr.push(i) if bool
		end
		temp_arr
	end

	def dictionarize dictionary
		@songs.each {|s| s.dictionarize(dictionary)}
	end
end
