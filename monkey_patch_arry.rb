class Array
	
	def to_hash
		hash = {}
		self.each do |arr|
			if arr.kind_of?(Array)
				hash[arr[0]] = arr[1]
			else
				puts "Wrong array structure"
				break
			end
		end
	hash	
	end

	def index_by &block
		hash = {}
		self.each do |element|
			temp = yield element
			hash[temp] = element
		end
		hash
	end

	def occurences_count
		hash = Hash.new(0)
		self.each do |element|
			hash[element] = 0 if hash[element] == nil
			hash[element] += 1
		end
		hash
	end

	def densities
		hash = self.occurences_count
		p
		tempArr = []
		self.each_index do |x|
			tempArr[x] = hash[self[x]]
		end
		tempArr
	end

	def subarray_count arg
		int = 0
		if arg.get
    	self.each_index do |x| 
    		if self[x] == arg[0] && self[x+1] == arg[1]
    			int += 1
    		end
    	end
    	int
	end
end

puts [1, 2, 3, 2, 3, 1].subarray_count([2, 3])
puts [1,2,1,2,1,2,3,3,4,1,2].subarray_count([1,2])
